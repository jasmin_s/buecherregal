import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse repraesentiert ein Buch-Objekt, welches aus
 * der Datenbank der Deutschen Nationalbibliothek ausgelesen wurde.
 *
 * @author jasmin_sander
 * @created 31.07.2017
 */
public class Book {

    String title;

    String publisher;

    LocalDate publishDate;

    List<String> authors = new ArrayList<>();

    List<String> subjects = new ArrayList<>();

    int pageNumber;

    public Book(String title) {
        this.title = title;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public boolean addSubject(String subject) {
        return subjects.add(subject);
    }

    public boolean removeSubject(String subject) {
        return subjects.remove(subject);
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public List<String> getAuthor() {
        return authors;
    }

    public boolean addAuthor(String author) {
        return authors.add(author);
    }

    public boolean removeAuthor(String author) {
        return authors.remove(author);
    }

    @Override
    public String toString() {
        return "Book: \"" + title + "\"";
    }


}

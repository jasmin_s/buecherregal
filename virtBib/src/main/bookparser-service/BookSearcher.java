import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * Diese Klasse repraesentiert die Suche nach den
 * Buechern in der Datenbank der Deutschen National Bibliothek.
 *
 * @author jasmin_sander
 * @created 24.07.2017
 */
public class BookSearcher{
    public final String SPACE = " ";

    private final String DNB_URL = "https://services.dnb.de/sru/accessToken~249492fa7ec1f46dd214e2d049c3a3e/dnb?version=1.1&operation=searchRetrieve&query=";
    private final String AUTHOR_KEY = "ATR%3D";
    private final String TITLE_KEY = "TIT%3D";
    private final String LINKED_SEARCH_SYMBOL = "%20and%20";
    public final String MAXIMUM_RECORDS_STRING = "&maximumRecords=100";
    public final String SPACE_SIGN = "%20";

    private int lastFoundEntries = 0;

    /**
     * Diese Methode stellt eine Suchanfrage an die Deutsche National Bibliothek.
     * Dabei wird nach dem Titel des Buches gesucht, wobei der String enthalten
     * sein muss.
     *
     * @param title
     * @return
     * @throws SystemException
     */
    public List<Book> searchByTitle(String title) throws SystemException {

        List<String> reply = sendRequest(
                DNB_URL + TITLE_KEY + title.replaceAll(SPACE, SPACE_SIGN) + MAXIMUM_RECORDS_STRING);
        return BookParser.parseXML(reply, this);
    }

    /**
     * Diese Methode stellt eine Suchanfrage an die Deutsche National Bibliothek.
     * Dabei wird nach dem Author und Titel des Buches gesucht, wobei der
     * String des Titels und des Autors enthalten sein muss.
     *
     * @param author
     * @param title
     * @return
     * @throws SystemException
     */
    public List<Book> searchByAuthorAndTitle(String author, String title) throws SystemException {
        List<String> reply = sendRequest(DNB_URL + TITLE_KEY + title.replaceAll(SPACE, SPACE_SIGN)
                + LINKED_SEARCH_SYMBOL + AUTHOR_KEY + author.replaceAll(SPACE, SPACE_SIGN) + MAXIMUM_RECORDS_STRING);
        return BookParser.parseXML(reply, this);
    }

    /**
     * Diese Methode stellt eine Suchanfrage an die Deutsche National Bibliothek.
     * Dabei wird nach dem Author des Buches gesucht, wobei der
     * String des Autors enthalten sein muss.
     *
     * @param author
     * @return
     * @throws SystemException
     */
    public List<Book> searchByAuthor(String author) throws SystemException {
        List<String> reply = sendRequest(
                DNB_URL + AUTHOR_KEY + author.replaceAll(SPACE, SPACE_SIGN) + MAXIMUM_RECORDS_STRING);
        return BookParser.parseXML(reply, this);
    }

    /**
     * Diese Methode sendet einen HTTP-Request.
     *
     * @param urlString
     * @return
     * @throws SystemException
     */
    private List<String> sendRequest(String urlString) throws SystemException {
        try {
            URL requestUrl = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) requestUrl.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            InputStream stream = conn.getInputStream();

            List<String> lines = XMLStreamHelper.parseStreamToListOfString(stream);

            return lines;

        } catch (IOException e) {
            throw new SystemException("I/O Error while contacting German National Library. Try again later!");
        } catch (ParserConfigurationException e) {
            throw new SystemException("Error while parsing XML. Contact Service.");
        } catch (SAXException e) {
            throw new SystemException("Error while parsing XML. Contact Service.");
        }
    }

    public int getLastFoundEntries() {
        return lastFoundEntries;
    }

    public void setLastFoundEntries(int lastFoundEntries) {
        this.lastFoundEntries = lastFoundEntries;
    }
}

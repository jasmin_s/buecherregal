/*
 * This file is part of a Werum IT Solutions GmbH project.
 *
 * Copyright (c)
 *    Werum IT Solutions GmbH
 *    All rights reserved.
 *
 * This source file may be managed in different Java package structures,
 * depending on actual usage of the source file by the Copyright holders:
 *
 * for Werum:  com.werum.* or any other Werum owned Internet domain
 *
 * Any use of this file as part of a software system by none Copyright holders
 * is subject to license terms.
 *  
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.org.apache.xerces.internal.dom.DeferredElementImpl;
import com.sun.org.apache.xerces.internal.dom.DeferredTextImpl;

/**
 * Description of class...
 *
 * @author jasmin_sander
 * @company Werum IT Solutions GmbH
 * @created 18.08.2017
 * @since PAS-X V3.2.1
 */
public class XMLStreamHelper {

    /**
     * Diese Methode schreibt die HTTP Antwort in eine Liste.
     *
     * @param stream
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static List<String>  parseStreamToListOfString(InputStream stream) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(stream);
        NodeList nodeList = doc.getElementsByTagName("*");

        List<String> lines = new ArrayList<>();
        lines = XMLStreamHelper.readNodeElementsFromXML(nodeList, lines);
        return lines;
    }

    /**
     * Diese Methode liest die XML-Elemente aus.
     *
     * @param nodeList
     * @param lines
     * @return
     */
    public static List<String> readNodeElementsFromXML(NodeList nodeList, List<String> lines) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node n;

            if (nodeList.item(i) instanceof DeferredElementImpl) {
                n = nodeList.item(i);
                String name = n.getNodeName();
                if (!name.trim().isEmpty())
                    lines.add(name);
                readNodeElementsFromXML(n.getChildNodes(), lines);
            }

            if (nodeList.item(i) instanceof DeferredTextImpl) {
                n = nodeList.item(i);
                String name = n.getTextContent();
                if (!name.trim().isEmpty())
                    lines.add(name);
                return lines;
            }
        }
        return lines;
    }

}

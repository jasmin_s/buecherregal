import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Diese Klasse repraesentiert den Parser fuer ein Buch aus einer Marc21-XML.
 *
 * @author jasmin_sander
 * @created 02.08.2017
 */
public class BookParser {

    public static final String KEY_NUMBER_OF_RECORDS = "numberOfRecords";
    public static final String KEY_CREATOR = "dc:creator";
    public static final String KEY_DATE = "dc:date";
    public static final String KEY_FORMAT = "dc:format";
    public static final String SEITEN_STRING = "Seiten";
    public static final String KEY_SUBJECT = "dc:subject";
    public static final String KEY_PUBLISHER = "dc:publisher";
    public static final String KEY_TITLE = "dc:title";
    public static final String KEY_END = "searchRetrieveResponse";

    /**
     * Diese Methode parst die gesamte Antwort der DNB und schickt
     * die Buecher-Teile der XML an den BookParser weiter. Zudem wird
     * die Anzahl der Eintraege gespeichert.
     *
     * @param XML
     * @return
     */
    public static List<Book> parseXML(List<String> XML, BookSearcher searcher) {
        List<Book> books = new ArrayList<>();
        int lastFoundEntries;

        for (int i = 0; i < XML.size(); i++) {
            String lineOfReply = XML.get(i);

            if (lineOfReply.equals(KEY_NUMBER_OF_RECORDS)) {
                lastFoundEntries = Integer.parseInt(XML.get(i + 1));
                searcher.setLastFoundEntries(lastFoundEntries);
                if (lastFoundEntries == 0)
                    return new ArrayList<>();
            }

            if (lineOfReply.equals(KEY_TITLE)) {
                Book book = BookParser.parseOneBook(XML.subList(i, XML.size()));
                books.add(book);
            }
        }

        return books;
    }

    /**
     * Diese Methode parset die XML-Datei, welche als Liste
     * der Methode uebergeben wird. Dabei wird nach Keys gesucht,
     * die bestimmte Bereiche in der XML kennzeichen.
     *
     * @param bookXML
     * @return
     */
    private static Book parseOneBook(List<String> bookXML) {
        Book book = new Book(bookXML.get(1));

        for (int i = 2; i < bookXML.size(); i++) {
            String lineOfReply = bookXML.get(i);

            parseAuthor(bookXML, book, i, lineOfReply);

            parsePublishDate(bookXML, book, i, lineOfReply);

            parsePageNumber(bookXML, book, i, lineOfReply);

            parseSubject(bookXML, book, i, lineOfReply);

            parsePublisher(bookXML, book, i, lineOfReply);

            if (recognizeEnd(lineOfReply))
                return book;
        }
        return book;
    }

    /**
     * Erkennt, ob ein neuer Titel erreicht wurde oder
     * das Ende der XML Datei gefunden worden ist.
     *
     * @param lineOfReply
     * @return
     */
    private static boolean recognizeEnd(String lineOfReply) {
        if (lineOfReply.equals(KEY_TITLE) || lineOfReply.equals(KEY_END))
            return true;
        return false;
    }

    private static void parseAuthor(List<String> XML, Book book, int i, String lineOfReply) {
        if (lineOfReply.equals(KEY_CREATOR)) {
            book.addAuthor(XML.get(i + 1));
        }
    }

    private static void parsePublisher(List<String> XML, Book book, int i, String lineOfReply) {
        if (lineOfReply.equals(KEY_PUBLISHER)) {
            book.setPublisher(XML.get(i + 1));
        }
    }

    private static void parseSubject(List<String> XML, Book book, int i, String lineOfReply) {
        if (lineOfReply.equals(KEY_SUBJECT)) {
            String line = XML.get(i + 1);
            if (line.contains(" "))
                book.addSubject(line.substring(line.indexOf(" ")).trim());
        }
    }

    private static void parsePageNumber(List<String> XML, Book book, int i, String lineOfReply) {
        if (lineOfReply.equals(KEY_FORMAT)) {
            String nextLine = XML.get(i + 1);
            if (nextLine.contains(SEITEN_STRING)) {
                Pattern pattern = Pattern.compile("(([\\d]+) " + SEITEN_STRING + ")");
                Matcher matcher = pattern.matcher(nextLine);
                if (matcher.find()) {
                    book.setPageNumber(Integer.parseInt(matcher.group(2)));
                }
            }
        }
    }

    private static void parsePublishDate(List<String> XML, Book book, int i, String lineOfReply) {
        if (lineOfReply.equals(KEY_DATE)) {
            String nextLine = XML.get(i + 1);
            Pattern pattern = Pattern.compile("([\\d]{4})");
            Matcher matcher = pattern.matcher(nextLine);
            if (matcher.find()) {
                book.setPublishDate(LocalDate.of(Integer.valueOf(matcher.group(1)), 1, 1));
            }
        }
    }
}

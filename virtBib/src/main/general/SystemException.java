
/**
 * Diese Klasse repraesentiert eine Exception,
 * die von unserem System verursacht worden ist.
 *
 * @author jasmin_sander
 * @created 03.08.2017
 */
public class SystemException extends Exception{
    public SystemException(String message) {
        super(message);
    }
}

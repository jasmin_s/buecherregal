import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Frame;
import com.leapmotion.leap.Hand;
import com.leapmotion.leap.HandList;
import com.leapmotion.leap.Listener;
import com.leapmotion.leap.Vector;

/**
 * Diese Klasse repraesentiert den Listener
 * der Leap Motion, der auf die Frames der
 * Leap Motion reagiert.
 *
 * @author jasmin_sander
 * @created 20.07.2017
 */
public class BibLeapListener extends Listener {
    public static final int FIRST_VALUE = 0;
    public static final int MAX_POSITIONS = 15;
    public static final double MIN_MOVEMENT_DISTANCE = 100.0;
    public static final int SECOND_VALUE = 1;

    private Map<Integer, List<Vector>> positionMap = new HashMap<>();
    private Map<Integer, List<Vector>> velocityMap = new HashMap<>();

    private boolean isClosedBookViewActivated = false;
    private boolean isOpenedBookViewActivated = false;

    private LeapMotionBibApplication app;

    public BibLeapListener(LeapMotionBibApplication app) {
        this.app = app;
    }

    @Override
    /**
     * Diese Methode reagiert auf jeden Frame des Leap Motion.
     * Dafuer wird je nachdem ob eine oder zwei Haende zu sehen
     * sind die Geste interpretiert.
     *
     * @param controller
     */
    public void onFrame(Controller controller) {
        Frame frame = controller.frame(FIRST_VALUE);
        HandList handList = frame.hands();

        if (handList.count() == 1) {

            Hand hand = handList.get(FIRST_VALUE);
            getHandValuesAndAddToMaps(hand);
            evaluateLastPositions(hand);

        } else if (handList.count() == 2) {
            Hand leftHand;
            Hand rightHand;

            if (handList.get(FIRST_VALUE).isLeft()) {
                leftHand = handList.get(FIRST_VALUE);
                rightHand = handList.get(SECOND_VALUE);
            } else {
                leftHand = handList.get(SECOND_VALUE);
                rightHand = handList.get(FIRST_VALUE);
            }

            getHandValuesAndAddToMaps(leftHand);
            getHandValuesAndAddToMaps(rightHand);

            evaluateLastPositions(leftHand, rightHand);
        }
    }

    /**
     * Diese Methode holt sich die Positions- und
     * Geschwindigkeitsvektoren der Hand.
     *
     * @param hand
     */
    private void getHandValuesAndAddToMaps(Hand hand) {
        int id = hand.id();
        Vector position = hand.palmPosition();
        Vector velocity = hand.palmVelocity();

        addPositionOrVelocityToMap(id, position, positionMap);
        addPositionOrVelocityToMap(id, velocity, velocityMap);
    }

    /**
     * Fuegt die Position oder Geschwindigkeit der Map hinzu.
     * Speichert sie anhand der ID.
     *
     * @param id
     * @param positionOrVelocity
     * @param positionOrVelocityMap
     */
    private void addPositionOrVelocityToMap(int id, Vector positionOrVelocity,
            Map<Integer, List<Vector>> positionOrVelocityMap) {
        List<Vector> lastValue = positionOrVelocityMap.get(id);
        if (lastValue == null)
            positionOrVelocityMap.put(id, new ArrayList<>());
        positionOrVelocityMap.get(id).add(positionOrVelocity);
    }

    /**
     * Diese Methode wertet die Handpositionen aus, wenn zwei Haende
     * erkannt worden sind und genug Frames festgehalten worden sind.
     *
     * @param leftHand
     * @param rightHand
     */
    private void evaluateLastPositions(Hand leftHand, Hand rightHand) {
        int leftHandId = leftHand.id();
        int rightHandId = rightHand.id();

        if (maximumSizeOfFramesReached(leftHandId, rightHandId)) {
            Vector firstValueLeft = positionMap.get(leftHandId).get(FIRST_VALUE);
            Vector lastValueLeft = positionMap.get(leftHandId).get(MAX_POSITIONS);

            Vector firstValueRight = positionMap.get(rightHandId).get(FIRST_VALUE);
            Vector lastValueRight = positionMap.get(rightHandId).get(MAX_POSITIONS);

            if (isOpenedBookViewActivated)
                if (isPush(firstValueLeft, lastValueLeft) && isPush(firstValueRight, lastValueRight))
                    handleClick();

            positionMap.remove(leftHandId);
            velocityMap.remove(leftHandId);
            positionMap.remove(rightHandId);
            velocityMap.remove(rightHandId);
        }
    }

    /**
     * Gibt zurueck, ob eine aussagekraeftige Nummer an Frames
     * fuer eine Hand festgehalten worden ist.
     *
     * @param leftHandId
     * @param rightHandId
     * @return
     */
    private boolean maximumSizeOfFramesReached(int leftHandId, int rightHandId) {
        return positionMap.get(leftHandId).size() > MAX_POSITIONS && positionMap.get(rightHandId).size() > MAX_POSITIONS;
    }

    /**
     * Diese Methode wertet die Handpositionen aus, wenn eine Hand
     * erkannt worden ist und genug Frames festgehalten worden sind.
     *
     * @param hand
     */
    private void evaluateLastPositions(Hand hand) {
        int id = hand.id();
        if (positionMap.get(id).size() > MAX_POSITIONS) {
            Vector firstValue = positionMap.get(id).get(FIRST_VALUE);
            Vector lastValue = positionMap.get(id).get(MAX_POSITIONS);

            if (isClosedBookViewActivated) {
                evaluateAndHandleTabAndScroll(hand, id, firstValue, lastValue);
            }

            positionMap.remove(id);
            velocityMap.remove(id);
        }
    }

    /**
     * Bewertet die unterschiedlichen Gesten fuer eine Hand.
     *
     * @param hand
     * @param id
     * @param firstValue
     * @param lastValue
     */
    private void evaluateAndHandleTabAndScroll(Hand hand, int id, Vector firstValue, Vector lastValue) {
        if (isSwipeRight(hand, firstValue, lastValue)) {
            handleScrollWithAverageVelocity(hand.isLeft(), id);

        } else if (isSwipeLeft(hand, firstValue, lastValue)) {
            handleScrollWithAverageVelocity(hand.isLeft(), id);

        } else if (isPull(hand, firstValue, lastValue))
            handleClick();
    }


    /**
     * Die Methode berechnet die durchschnittliche Geschwindigkeit der
     * Bewegung und ruft das Scrollen in der App auf.
     *
     * @param handIsLeft
     * @param id
     */
    private void handleScrollWithAverageVelocity(boolean handIsLeft, int id) {
        double averageSpeed = velocityMap.get(id).stream().mapToDouble(vector -> vector.getX()).average().getAsDouble();
        handleScroll(Math.abs(averageSpeed), !handIsLeft);
    }

    /**
     * Diese Methode fuehrt das Scrollen in der App aus.
     *
     * @param handSpeed
     * @param toRight
     */
    private void handleScroll(double handSpeed, boolean toRight) {
        try {
            app.scroll(handSpeed, toRight);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Diese Methode fuehrt einen Klick in der App aus.
     */
    private void handleClick() {
        app.click();
    }

    private boolean isPush(Vector firstValue, Vector lastValue) {
        return firstValue.getZ() - lastValue.getZ() >= MIN_MOVEMENT_DISTANCE;
    }

    private boolean isPull(Hand hand, Vector firstValue, Vector lastValue) {
        return (lastValue.getZ() - firstValue.getZ() >= 1.5 * MIN_MOVEMENT_DISTANCE
                && hand.fingers().extended().count() < 3);
    }

    private boolean isSwipeLeft(Hand hand, Vector firstValue, Vector lastValue) {
        return firstValue.getX() - lastValue.getX() >= MIN_MOVEMENT_DISTANCE && hand.isRight()
                && hand.fingers().extended().count() > 3;
    }

    private boolean isSwipeRight(Hand hand, Vector firstValue, Vector lastValue) {
        return lastValue.getX() - firstValue.getX() >= MIN_MOVEMENT_DISTANCE && hand.isLeft()
                && hand.fingers().extended().count() > 3;
    }
    public void setClosedBookViewActivated(boolean closedBookViewActivated) {
        isClosedBookViewActivated = closedBookViewActivated;
    }

    public void setOpenedBookViewActivated(boolean openedBookViewActivated) {
        isOpenedBookViewActivated = openedBookViewActivated;
    }

}

import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

/**
 * Diese Klasse stellt den Controller fuer die Startseite dar.
 *
 * @author jasmin_sander
 * @created 03.08.2017
 */
public class LibraryMainPageController {
    public static final String OPEN_BOOK_FXML_PATH = "virtBib/src/main/library-ui/fxml/openBook.fxml";
    public static final String BOOK_FXML_PATH = "virtBib/src/main/library-ui/fxml/book.fxml";
    private static final String LOAD_WINDOW_FXML_PATH = "virtBib/src/main/library-ui/fxml/loadingBooks.fxml";

    private LeapMotionBibApplication mainApp;

    @FXML
    private Label searchingLabel;

    @FXML
    private ImageView helpPerson;

    @FXML
    private Text helpText;

    @FXML
    private Button closeWelcomeButton;

    @FXML
    private Text speachWelcome;

    @FXML
    private ImageView peopleWelcome;

    @FXML
    private ImageView speachBubbleWelcome;

    @FXML
    private ImageView speachbubble;

    @FXML
    private TextField titleField;

    @FXML
    private TextField authorField;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private TilePane tile;

    @FXML
    private Label ergebnisLabel;

    private BookSearcher searcher;

    private ProgressBar bar;

    private Stage openedBookStage;

    private ObservableList<Book> buecher = FXCollections.observableArrayList();

    private SimpleDoubleProperty searchTime = new SimpleDoubleProperty(0.0);

    protected void setMainApp(LeapMotionBibApplication mainApp) {
        this.mainApp = mainApp;
    }

    @FXML
    private void initialize() {
        searcher = new BookSearcher();
        tile.setPadding(new Insets(5, 5, 5, 5));
        scrollPane.setContent(tile);
    }

    /**
     * Diese Methode setzt den Fokus auf das Titel-Such-Feld.
     */
    protected void setFocus() {
        titleField.requestFocus();
    }

    /**
     * Diese Methode fuehrt eine Suche aus und schliesst die Hilfe-Dialoge.
     */
    public void handleSearch() {
        try {
            closeWelcomeDialog();
            closeSearchHelpDialog();
            performBookSearch(titleField.getText(), authorField.getText());

        } catch (SystemException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Diese Methode schliesst die Informationen zur Buchsuche.
     *
     * @param informationStage
     */
    private void closeBookSearchingInformation(Stage informationStage) {
        informationStage.close();
    }

    /**
     * Diese Methode oeffnet die Fortschrittsanzeige bei der Buchsuche.
     *
     * @return
     * @throws IOException
     */
    private Stage openBookSearchingInformation() throws IOException {
        File fxmlFile = new File(LOAD_WINDOW_FXML_PATH);
        FXMLLoader fxml = new FXMLLoader(fxmlFile.toURI().toURL());
        Parent root = fxml.load();

        bar = (ProgressBar) fxml.getNamespace().get("progressBar");

        Scene scene = new Scene(root, 531, 330);

        Stage dialogStage;

        // Stage konfigurieren
        dialogStage = new Stage();
        dialogStage.setScene(scene);
        dialogStage.setResizable(false);
        dialogStage.show();

        return dialogStage;
    }

    @FXML
    /**
     * Diese Methode fuehrt bei Eingabe von Enter eine Suche aus.
     *
     * @param event
     */
    public void handleSearchByClickEnterButton(KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            searchingLabel.setVisible(true);

            handleSearch();

            searchingLabel.setVisible(false);
        }
    }

    @FXML
    /**
     * Diese Methode oeffnet die Such-Hilfe.
     */
    public void openSearchHelp() {
        speachbubble.setVisible(true);
        helpText.setVisible(true);
        helpPerson.setVisible(true);
        closeWelcomeDialog();
    }

    @FXML
    /**
     * Diese Methode schliesst den Wilkommens-Hilfe-Dialog.
     */
    public void closeWelcomeDialog() {
        closeWelcomeButton.setVisible(false);
        peopleWelcome.setVisible(false);
        speachBubbleWelcome.setVisible(false);
        speachWelcome.setVisible(false);
    }

    /**
     * Diese Methode schliesst den Such-Hilfe-Dialog.
     */
    public void closeSearchHelpDialog() {
        speachbubble.setVisible(false);
        helpText.setVisible(false);
        helpPerson.setVisible(false);
    }

    /**
     * Diese Methode fuehrt eine Suche aus.
     *
     * @param title
     * @param author
     * @throws SystemException
     * @throws IOException
     */
    private void performBookSearch(String title, String author) throws SystemException, IOException {
        buecher = FXCollections.emptyObservableList();

        mainApp.getLeapListener().setClosedBookViewActivated(false);

        Stage informationStage = openBookSearchingInformation();

        Service service = getSearchService(title, author);

        Service timeService = getTimeService();

        bar.progressProperty().bind(searchTime);
        service.start();
        timeService.start();

        handleSearchServiceSucceed(informationStage, service, timeService);

    }

    /**
     * Diese Methode setzt die GUI zurueck, wenn die Suche erfolgreich war.
     *
     * @param informationStage
     * @param service
     * @param timeService
     */
    private void handleSearchServiceSucceed(Stage informationStage, Service service, Service timeService) {
        service.setOnSucceeded(event -> {
            StringBuilder builder = new StringBuilder();
            builder.append("There were ").append(searcher.getLastFoundEntries()).append(" results for your search.");
            ergebnisLabel.setText(builder.toString());

            try {
                setupBooksForGUI();
            } catch (IOException e) {
                e.printStackTrace();
            }

            timeService.cancel();

            searchTime.set(1.0);

            scrollPane.setHvalue(0.0);

            ergebnisLabel.setVisible(true);

            closeBookSearchingInformation(informationStage);

            mainApp.getLeapListener().setClosedBookViewActivated(true);
        });
    }

    /**
     * Diese Methode erstellt den Zeit-Service zur Verfolgung des Fortschritts.
     *
     * @return
     */
    Service getTimeService() {
        return new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        searchTime.set(0.0);
                        for (int i = 0; i < 100; i++) {
                            Thread.sleep(20); //PC: 10 = 1sec
                            searchTime.set(searchTime.get() + 0.01);
                        }
                        return null;

                    }
                };
            }
        };
    }

    /**
     * Diese Methode erstellt den Such-Service.
     *
     * @param title
     * @param author
     * @return
     */
    private Service getSearchService(String title, String author) {
        return new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {

                        if (author.isEmpty()) {
                            buecher = FXCollections.observableArrayList(searcher.searchByTitle(title));
                        } else if (title.isEmpty()) {
                            buecher = FXCollections.observableArrayList(searcher.searchByAuthor(author));
                        } else {
                            buecher = FXCollections.observableArrayList(searcher.searchByAuthorAndTitle(author, title));
                        }

                        return null;

                    }
                };
            }
        };
    }

    /**
     * Diese Methode erstellt die Buecher fuer die Suche.
     *
     * @throws IOException
     */
    private void setupBooksForGUI() throws IOException {
        tile.getChildren().removeAll(tile.getChildren());

        for (int i = 0; i < buecher.size(); i++) {

            Book buch = buecher.get(i);

            File fxmlFile = new File(BOOK_FXML_PATH);
            FXMLLoader fxml = new FXMLLoader(fxmlFile.toURI().toURL());
            fxml.load();

            StackPane stack = (StackPane) fxml.getNamespace().get("bookStackPane");
            AnchorPane anchor = (AnchorPane) fxml.getNamespace().get("anchor");
            Label titleText = (Label) fxml.getNamespace().get("titleField");
            Label authorText = (Label) fxml.getNamespace().get("authorField");

            titleText.setText(buch.getTitle());

            setAuthorLabel(buch, authorText);

            Book book = buecher.get(i);

            anchor.setOnMouseClicked(event -> {
                handleOpenBookDetailView(book);
            });

            tile.getChildren().add(stack);
        }
    }

    /**
     * Diese Methode setzt das Autor-Label.
     *
     * @param buch
     * @param authorText
     */
    protected void setAuthorLabel(Book buch, Label authorText) {
        StringBuilder builder = new StringBuilder();
        List<String> authors = buch.getAuthor();
        for (int u = 0; u < authors.size(); u++) {
            if (u < authors.size() - 1)
                builder.append(authors.get(u) + "\n");
            else
                builder.append(authors.get(u));
        }
        authorText.setText(builder.toString());
    }

    /**
     * Diese Methode laedt ein offenes Buch.
     *
     * @param book
     */
    private void handleOpenBookDetailView(Book book) {
        try {
            File fxmlFile = new File(OPEN_BOOK_FXML_PATH);
            FXMLLoader fxml = new FXMLLoader(fxmlFile.toURI().toURL());
            Parent root = fxml.load();

            LibraryOpenBookController openBookController = fxml.getController();
            openBookController.setMainController(this);
            openedBookStage = openBookController.handleInitializeOpenedBook(book, root);

            openedBookStage.show();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Stage getOpenedBookStage() {
        return openedBookStage;
    }

    public LeapMotionBibApplication getMainApp() {
        return mainApp;
    }

    public ObservableList<Book> getBuecher() {
        return buecher;
    }
}

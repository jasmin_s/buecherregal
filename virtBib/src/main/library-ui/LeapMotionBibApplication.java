import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.leapmotion.leap.Controller;

/**
 * Diese Klasse startet die Applikation und laedt die GUI.
 * Zudem stellt sie grundlegende Funktionen dar wie das Scrollen und das Klicken.
 *
 * @author karl-philipp_krueger
 * @created 24.07.2017
 */
public class LeapMotionBibApplication extends Application {
    public static final double MIN_VELOCITY = 800.0;
    private final double SCROLLBAR_MAX = 1.0;
    private final double SCROLLBAR_MIN = 0.0;
    private final int SPEEDMAX = 5000;
    private final double NORMALIZE_FACTOR = Math.log10(SPEEDMAX);
    private final int SLOW_DOWN_FACTOR = 50;
    private final int SCROLL_UPDATE_DELAY = 50;
    private final double VELOCITY_THRESHOLD = 0.001;
    private final int WINDOW_WIDTH = 1218;
    private final int WINDOW_HEIGHT = 820;
    private final String APPLICATION_TITLE = "VLib - Your Virtual Library";
    private final String SCROLL_PANE_ID = "scrollPane";
    private final String MAIN_FXML_PATH = "virtBib/src/main/library-ui/fxml/mainPage.fxml";

    private Controller leapController;

    private LibraryMainPageController libraryController;

    private BibLeapListener leapListener;

    private ScrollPane scrollPane;

    private Thread runningScroll;

    private Thread runningTab;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Diese Methode startet die Applikation, laedt die GUI aus
     * der FXML und initialisiert die Controller.
     *
     * @param primaryStage
     * @throws InterruptedException
     * @throws IOException
     * @throws SystemException
     */
    @Override
    public void start(Stage primaryStage) throws InterruptedException, IOException, SystemException {
        Parent root = loadFXML();

        libraryController.setMainApp(this);

        setupLeapController();

        configurePrimaryStage(primaryStage, root);

        libraryController.setFocus();
    }

    /**
     * Diese Methode laedt die Hauptseite aus einer FXML
     * und liest Controller und Scrollbar aus.
     *
     * @return
     * @throws IOException
     */
    private Parent loadFXML() throws IOException {
        File fxmlFile = new File(MAIN_FXML_PATH);
        FXMLLoader fxml = new FXMLLoader(fxmlFile.toURI().toURL());
        Parent root = fxml.load();

        scrollPane = (ScrollPane) fxml.getNamespace().get(SCROLL_PANE_ID);
        libraryController = fxml.getController();

        return root;
    }

    /**
     * Diese Methode konfiguriert die Startseite und zeigt sie an.
     *
     * @param primaryStage
     * @param root
     */
    private void configurePrimaryStage(Stage primaryStage, Parent root) {
        Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);

        primaryStage.setTitle(APPLICATION_TITLE);
        primaryStage.setScene(scene);
        primaryStage.setMaximized(false);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /**
     * Diese Methode startet den Leap Listener und den Controller.
     */
    private void setupLeapController() {
        leapController = new Controller();
        leapListener = new BibLeapListener(this);
        leapController.addListener(leapListener);
    }

    /**
     * Diese Methode setzt die Scrollbar abhaengig von
     * der Gestengeschwindigkeit und der Richtung.
     *
     * @param gestureSpeed
     * @param toRight
     * @throws InterruptedException
     */
    public void scroll(double gestureSpeed, boolean toRight) throws InterruptedException {

        Task task = new Task<Boolean>() {
            @Override
            public Boolean call() {
                double scrollVelocity;

                if (gestureSpeed > MIN_VELOCITY) {

                    scrollVelocity = getScrollVelocity(gestureSpeed, toRight);

                    while (Math.abs(scrollVelocity) >= VELOCITY_THRESHOLD && !(runningScroll.isInterrupted())) {

                        double newScrollValue = getNewScrollValue(scrollVelocity);

                        scrollPane.setHvalue(newScrollValue);

                        try {
                            TimeUnit.MILLISECONDS.sleep(SCROLL_UPDATE_DELAY);
                        } catch (InterruptedException e) {
                            runningScroll.interrupt();
                            return false;
                        }

                        scrollVelocity = calculateDecay(scrollVelocity);
                    }
                } else {
                    handleScrollOneBook(toRight);
                }

                libraryController.setFocus();

                return true;
            }
        };

        startThreadIfNoneIsRunning(task);
    }

    /**
     * Diese Methode startet den Scroll-Thread, wenn kein
     * Scrolling laeuft.
     *
     * @param task
     */
    private void startThreadIfNoneIsRunning(Task task) {
        if (runningScroll != null && runningScroll.isAlive())
            runningScroll.interrupt();

        runningScroll = new Thread(task);
        runningScroll.start();
    }

    /**
     * Diese Methode scrollt, wenn die Geschwindigkeit kleiner ist als
     * 800 mm/s. Dabei wird ein Buch weit gescrollt.
     *
     * @param toRight
     */
    private void handleScrollOneBook(boolean toRight) {
        double scrollVelocity;
        int totalNumberOfBooks = libraryController.getBuecher().size();

        scrollVelocity = getScrollVelocityOneBook(totalNumberOfBooks, toRight);

        double newScrollValue = getNewScrollValue(scrollVelocity);

        scrollPane.setHvalue(newScrollValue);
    }

    /**
     * Diese Methode bestimmt die Scrollgeschwindigkeit fuer ein Buch.
     *
     * @param totalNumberOfBooks
     * @param toRight
     * @return
     */
    private double getScrollVelocityOneBook(int totalNumberOfBooks, boolean toRight) {
        double scrollVelocity;
        scrollVelocity = 1.0 / totalNumberOfBooks;

        scrollVelocity = toRight ? scrollVelocity : (-scrollVelocity);
        return scrollVelocity;
    }

    /**
     * Diese Methode bestimmt die normale Scrollgeschwindigkeit.
     *
     * @param gestureSpeed
     * @param toRight
     * @return
     */
    private double getScrollVelocity(double gestureSpeed, boolean toRight) {
        double scrollVelocity;
        scrollVelocity = Math.log10(gestureSpeed) / NORMALIZE_FACTOR / SLOW_DOWN_FACTOR;
        scrollVelocity = toRight ? scrollVelocity : (-scrollVelocity);

        return scrollVelocity;
    }

    /**
     * Diese Methode bestimmt den neuen Scrollwert.
     *
     * @param scrollVelocity
     * @return
     */
    private double getNewScrollValue(double scrollVelocity) {
        double newScrollValue = scrollPane.getHvalue() + scrollVelocity;
        newScrollValue = (newScrollValue > SCROLLBAR_MAX) ? SCROLLBAR_MAX : newScrollValue;
        newScrollValue = (newScrollValue < SCROLLBAR_MIN) ? SCROLLBAR_MIN : newScrollValue;
        return newScrollValue;
    }

    /**
     * Diese Methode bestimmt die Verzoegerung, um die Scrollweite zu verlangsamen.
     *
     * @param scrollVelocity
     * @return
     */
    private double calculateDecay(double scrollVelocity) {
        return scrollVelocity * 0.8;
    }

    /**
     * Diese Methode fuehrt mit Hilfe eines Roboters einen Klick aus.
     * Der Klick findet in der Mitte des Bildschirms statt.
     *
     */
    public void click() {

        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        int width = gd.getDisplayMode().getWidth();
        int height = gd.getDisplayMode().getHeight();

        Task task = new Task<Boolean>() {
            @Override
            public Boolean call() {
                Point originalLocation = java.awt.MouseInfo.getPointerInfo().getLocation();
                try {
                    Robot robot = new java.awt.Robot();
                    robot.mouseMove(width / 2, height / 2);
                    robot.mousePress(InputEvent.BUTTON1_MASK);
                    robot.mouseRelease(InputEvent.BUTTON1_MASK);
                    robot.mouseMove((int) originalLocation.getX(), (int) originalLocation.getY());
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                libraryController.setFocus();
                return true;
            }
        };

        if (runningTab == null || !runningTab.isAlive()) {
            runningTab = new Thread(task);
            runningTab.start();
        }
    }

    public BibLeapListener getLeapListener() {
        return leapListener;
    }
}

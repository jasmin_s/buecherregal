import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Diese Klasse stellt den Controller fuer den Opened Book View dar.
 *
 * @author jasmin_sander
 * @created 03.08.2017
 */
public class LibraryOpenBookController {

    Stage openedBookStage;

    private LibraryMainPageController mainController;

    public static final int OPENEDBOOK_WIDTH = 863;
    public static final int OPENEDBOOK_HEIGHT = 565;
    public static final String OPENEDBOOK_TITLE = "Open Book View";

    @FXML
    private Label publisherLabel;

    @FXML
    private Label titleLabel;

    @FXML
    private Label authorLabel;

    @FXML
    private Label pageNumberLabel;

    @FXML
    private Label publishDateLabel;

    @FXML
    private Label subjectLabel;

    @FXML
    /**
     * Diese Methode schliesst die Open-Book Ansicht.
     */
    public void handleCloseOpenedBook() {
        openedBookStage.fireEvent(new WindowEvent(openedBookStage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    protected Stage handleInitializeOpenedBook(Book book, Parent root) {

        setupBookLabels(book, titleLabel, authorLabel, publisherLabel, publishDateLabel, pageNumberLabel);

        StringBuilder builder = loadBookSubjects(book);
        subjectLabel.setText(builder.length() == 0 ? "" : builder.toString());

        BibLeapListener listener = setLeapListenerForOpenedBook();

        return initializeOpenedBookStage(root, listener);
    }

    /**
     * Diese Methode setzt die Label fuer die Buch-Informationen.
     *
     * @param book
     * @param titleLabel
     * @param authorLabel
     * @param publisherLabel
     * @param publishDateLabel
     * @param pageNumberLabel
     */
    private void setupBookLabels(Book book, Label titleLabel, Label authorLabel, Label publisherLabel,
                                 Label publishDateLabel, Label pageNumberLabel) {
        titleLabel.setText(book.getTitle());
        mainController.setAuthorLabel(book, authorLabel);
        publisherLabel.setText(book.getPublisher() == null ? "none" : book.getPublisher());
        publishDateLabel
                .setText(book.getPublishDate() == null ? "none" : String.valueOf(book.getPublishDate().getYear()));
        pageNumberLabel.setText(book.getPageNumber() == 0 ? "unknown" : String.valueOf(book.getPageNumber()));
    }

    /**
     * Diese Methode initialisiert die opened book stage.
     *  @param root
     * @param listener
     */
    private Stage initializeOpenedBookStage(Parent root, BibLeapListener listener) {
        openedBookStage = new Stage();
        openedBookStage.setTitle(OPENEDBOOK_TITLE);
        openedBookStage.setScene(new Scene(root, OPENEDBOOK_WIDTH, OPENEDBOOK_HEIGHT));

        openedBookStage.setOnCloseRequest(event -> {
            listener.setClosedBookViewActivated(true);
            listener.setOpenedBookViewActivated(false);
        });

        return openedBookStage;
    }

    /**
     * Diese Methode laedt die Themen des Buches.
     *
     * @param book
     * @return
     */
    private StringBuilder loadBookSubjects(Book book) {
        StringBuilder builder = new StringBuilder();
        List<String> subjects = book.getSubjects();
        for (int u = 0; u < subjects.size() - 1; u++) {
            if (u < subjects.size() - 2)
                builder.append("- " + subjects.get(u) + "\n");
            else
                builder.append("- " + subjects.get(u));
        }
        return builder;
    }

    /**
     * Diese Methode legt den Controller fest und holt sich die
     * Opened Book Stage.
     *
     * @param mainController
     */
    public void setMainController(LibraryMainPageController mainController) {
        this.mainController = mainController;
    }


    /**
     * Diese Methode initialisiert den Bib Listener.
     *
     * @return
     */
    private BibLeapListener setLeapListenerForOpenedBook() {
        BibLeapListener listener = mainController.getMainApp().getLeapListener();
        listener.setClosedBookViewActivated(false);
        listener.setOpenedBookViewActivated(true);
        return listener;
    }
}
